--------------------------------------------------------
-- Minetest :: Basic Ownership Mod v2.0 (ownership)
--
-- See README.txt for licensing and other information.
-- Copyright (c) 2016-2017, Leslie Ellen Krause
--
-- ./games/just_test_tribute/mods/ownership/init.lua
--------------------------------------------------------

default.OWNER_ANYBODY = "_anybody"
default.OWNER_SOMEBODY = "_somebody"
default.OWNER_NOBODY = ""	-- do not change this value!

default.STATUS_CONTAINER_GET = "%s moves stuff from %s at %s"
default.STATUS_CONTAINER_DEL = "%s removes stuff from %s at %s"
default.STATUS_CONTAINER_PUT = "%s moves stuff to %s at %s"
default.STATUS_CONTAINER_SET = "%s moves stuff in %s at %s"
default.STATUS_SIGNATURE_SET = "%s wrote \"%s\" to %s at %s"

minetest.register_on_prejoinplayer( function ( name, ip )
        local data, auth, cname
        local uname = string.lower( name )
        local exp_days = 180

        if minetest.setting_get( "halt_message" ) then
                return minetest.setting_get( "halt_message" )
        end

        if uname == default.OWNER_ANYBODY or uname == default.OWNER_SOMEBODY or
	   uname == default.OWNER_NOBODY or string.byte( uname ) == 95 then
                return "This player name is reserved and cannot be used."
        end
end )

-----------------------------------------------------------------
-- verify player's ownership of node at given position
-- allow for customized methods by abstracting node meta
-----------------------------------------------------------------

default.set_owner = function ( pos, owner )
	minetest.get_meta( pos ):set_string( "owner", owner )
end

default.is_owner = function ( pos, player )
	local owner = minetest.get_meta( pos ):get_string( "owner" )

	--minetest.chat_send_player( name, "This area belongs to " .. owner .. "." )
	return minetest.check_player_privs( player:get_player_name( ), { protection_bypass = true } ) or 
		owner == default.OWNER_ANYBODY or
		owner == player:get_player_name( ) or
		owner == default.OWNER_NOBODY and not minetest.is_protected( pos, player ) 
end

-----------------------------------------------------------------
-- determine if inventory of container is empty
-----------------------------------------------------------------

default.is_empty = function ( pos, list )
	return minetest.get_meta( pos ):get_inventory( ):is_empty( list or "main" )
end

-----------------------------------------------------------------
-- manage overall inventory of container
-----------------------------------------------------------------

default.get_contents = function ( chest_inv, player_inv, list )
	local slot, item
	for slot = 1, chest_inv:get_size( list or "main" ) do
		item = chest_inv:get_stack( list or "main", slot )
		if player_inv:room_for_item( "main", item ) then
			chest_inv:set_stack( list or "main", slot, nil )
			player_inv:add_item( "main", item )
		end
	end
end

default.put_contents = function ( chest_inv, player_inv, list )
	local slot, item
	for slot = 1, player_inv:get_size( "main" ) do
		item = player_inv:get_stack( "main", slot )
		if chest_inv:room_for_item( list or "main", item ) then
			player_inv:set_stack( "main", slot, nil )
			chest_inv:add_item( list or "main", item )
		end
	end
end

default.del_contents = function ( chest_inv, list )
	chest_inv:set_list( list or "main", { } )
end
